import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from "react-redux";
import {StatusBar} from 'react-native';
import AppStore from "./src/store/AppStore";
import AppStackNavigator from "./src/navigators/AppStackNavigator";
import {Colors} from "./src/styles/Colors";

const App = () => {
    return (
        <Provider store={AppStore}>
            <StatusBar
                backgroundColor={Colors.PRIMARY_COLOR}
                barStyle="light-content"
            />
            <AppStackNavigator/>
        </Provider>
    );
};

export default App;
