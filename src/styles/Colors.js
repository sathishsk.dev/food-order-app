export const Colors = {
    PRIMARY_COLOR: '#111e2d',
    ACCENT_COLOR: '#e3ab3d',
    WHITE: '#ffffff',
    BLACK: '#000000',
    BACKGROUND_COLOR: '#fcfcfc',
    SHADOW_COLOR: '#00000029',
    TITLE_COLOR: '#292c46',
    TEXT_COLOR: '#3a4449',
    GREY: '#f2F4f5',
    SECONDARY_TEXT_COLOR: '#5F717B',
    MODAL_BACKDROP_COLOR: '#000000A6',
    TRANSPARENT: 'transparent'
};