import {StyleSheet} from 'react-native';
import {Colors} from "./Colors";

const CommonStyles = StyleSheet.create({
    flexRow: {
        flexDirection: 'row'
    },
    flexRowReverse: {
        flexDirection: 'row-reverse'
    },
    flex: {
        flex: 1
    },
    centered: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    shadowView: {
        shadowColor: Colors.SHADOW_COLOR,
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 3,
        shadowOpacity: 1,
        elevation: 4
    },
    separator: {
        width: '100%',
        height: 8,
        backgroundColor: Colors.GREY
    }
});

export default CommonStyles;
