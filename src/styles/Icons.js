const Icons = {
    CART: "cart",
    FAVORITE: "favorite",
    MINUS: "less",
    PLUS: "plus",
    PHONE: "phone",
    MENU: "menu",
    EMPTY: "empty"
};

export default Icons;