import {combineReducers} from "redux";
import CartReducer from "./CartReducer";

const AppReducer = combineReducers({CartReducer});

export default AppReducer;