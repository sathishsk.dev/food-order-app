import {createStore, applyMiddleware} from 'redux'
import AppReducer from "./AppReducer";
import thunk from 'redux-thunk';

const AppStore = createStore(AppReducer, applyMiddleware(thunk));

export default AppStore
