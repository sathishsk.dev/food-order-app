import {
    ADD_QUANTITY,
    SUBTRACT_QUANTITY
} from "../actions/ActionTypes";

const initialState = {
    foodItems: [
        {
            "id": 1,
            "category": "Starters",
            "foodName": "Chicken Lollipop",
            "price": 10,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 2,
            "category": "Starters",
            "foodName": "Boneless Chicken 65",
            "price": 12,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 3,
            "category": "Starters",
            "foodName": "Fish 65",
            "price": 14,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 4,
            "category": "Starters",
            "foodName": "Prawns 65",
            "price": 15,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 5,
            "category": "Starters",
            "foodName": "Vanjaram Fish Fry",
            "price": 10,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 6,
            "category": "Starters",
            "foodName": "Chicken Pepper Fry",
            "price": 8,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 7,
            "category": "Starters",
            "foodName": "Chicken Lollipop Saucy",
            "price": 18,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 25,
            "category": "Starters",
            "foodName": "Chicken Kabab",
            "price": 8,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 26,
            "category": "Starters",
            "foodName": "Mutton Chukka",
            "price": 18,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 8,
            "category": "Main course",
            "foodName": "Chicken Biryani",
            "price": 15,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 9,
            "category": "Main course",
            "foodName": "Prawn Biryani",
            "price": 12,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 10,
            "category": "Main course",
            "foodName": "Mutton Biryani",
            "price": 13,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 11,
            "category": "Main course",
            "foodName": "Chicken Dum Biryani",
            "price": 19,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 12,
            "category": "Main course",
            "foodName": "Paneer Butter Masala",
            "price": 15,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 13,
            "category": "Main course",
            "foodName": "Chicken Gravy",
            "price": 15,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 14,
            "category": "Main course",
            "foodName": "Chicken Chettinadu Masala",
            "price": 13,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 15,
            "category": "Main course",
            "foodName": "Fish Masala",
            "price": 16,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 27,
            "category": "Main course",
            "foodName": "Veg Biryani",
            "price": 13,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 28,
            "category": "Main course",
            "foodName": "Mushroom Masala",
            "price": 16,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 16,
            "category": "Dessert",
            "foodName": "Choco Lava Cake",
            "price": 5,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 17,
            "category": "Dessert",
            "foodName": "Butterscotch Cake",
            "price": 6,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 15,
            "category": "Dessert",
            "foodName": "Rich Chocolate",
            "price": 3,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 18,
            "category": "Dessert",
            "foodName": "Moments",
            "price": 5,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 19,
            "category": "Dessert",
            "foodName": "Cheese Cake",
            "price": 8,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 20,
            "category": "Dessert",
            "foodName": "Hazelnut Brownie",
            "price": 7,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 30,
            "category": "Dessert",
            "foodName": "Black Forest",
            "price": 8,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 32,
            "category": "Dessert",
            "foodName": "White Forest",
            "price": 7,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 21,
            "category": "Drinks",
            "foodName": "Jeera Masala",
            "price": 4,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 22,
            "category": "Drinks",
            "foodName": "Green Apple",
            "price": 5,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 23,
            "category": "Drinks",
            "foodName": "Mint Chass",
            "price": 6,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 24,
            "category": "Drinks",
            "foodName": "Lemon Mint",
            "price": 3,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 33,
            "category": "Drinks",
            "foodName": "Apple Juice",
            "price": 6,
            "currency": "$",
            "quantity": 0
        },
        {
            "id": 34,
            "category": "Drinks",
            "foodName": "Mango Juice",
            "price": 3,
            "currency": "$",
            "quantity": 0
        },
    ],
    cartItems: [],
    totalCost: 0
};

const CartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_QUANTITY: {
            let addedItem = state.foodItems.find(item => item.id === action.id);
            let existedItem = state.cartItems.find(item => action.id === item.id);
            if (existedItem) {
                addedItem.quantity += 1;
                return {
                    ...state,
                    totalCost: state.totalCost + addedItem.price
                }
            } else {
                // Add item in cart
                addedItem.quantity = 1;
                let newTotal = state.totalCost + addedItem.price;
                return {
                    ...state,
                    cartItems: [...state.cartItems, addedItem],
                    totalCost: newTotal
                }
            }
        }

        case SUBTRACT_QUANTITY: {
            let addedItem = state.foodItems.find(item => item.id === action.id);
            if (addedItem.quantity === 1) {
                // Remove item from cart
                let newItems = state.cartItems.filter(item => item.id !== action.id);
                let newTotal = state.totalCost - addedItem.price;
                addedItem.quantity = 0;
                return {
                    ...state,
                    cartItems: newItems,
                    totalCost: newTotal
                }
            } else {
                addedItem.quantity -= 1;
                let newTotal = state.totalCost - addedItem.price;
                return {
                    ...state,
                    totalCost: newTotal
                }
            }
        }

        default:
            return state;
    }
};

export default CartReducer;