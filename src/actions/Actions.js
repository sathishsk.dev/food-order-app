import {
    ADD_QUANTITY,
    SUBTRACT_QUANTITY
} from "./ActionTypes";

export const subtractQuantity = id => {
    return {
        type: SUBTRACT_QUANTITY,
        id
    }
};

export const addQuantity = id => {
    return {
        type: ADD_QUANTITY,
        id
    }
};