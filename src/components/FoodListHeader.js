import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Colors} from "../styles/Colors";

const FoodListHeader = ({title}) => {
    return (
        <View style={styles.container}>
            <Text style={styles.titleText}>
                {title}
            </Text>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        padding: 16
    },
    titleText: {
        color: Colors.TITLE_COLOR,
        fontSize: 18,
        fontWeight: "bold"
    }
});


export default FoodListHeader;