import React from 'react';
import {createIconSetFromFontello} from 'react-native-vector-icons';
import FontIcons from '../../config';

const AppIcon = ({name, size, color}) => {

    const fontImagesFileName = 'fontImages';
    const Icon = createIconSetFromFontello(
        FontIcons,
        fontImagesFileName
    );

    return (
        <Icon
            name={name}
            size={size}
            color={color}
        />
    )
};

export default AppIcon;
