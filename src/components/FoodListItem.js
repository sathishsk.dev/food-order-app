import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Colors} from "../styles/Colors";
import AddButton from "./AddButton";

const FoodListItem = (props) => {
    const {foodName, price, currency, quantity} = props.item;

    const disableButton = quantity > 0;
    const text = disableButton ? quantity : "ADD";
    return (
        <View style={styles.container}>
            <View style={styles.nameContainer}>
                <Text style={styles.titleText}>
                    {foodName}
                </Text>
                <Text style={styles.priceText}>
                    {`${currency}${price}`}
                </Text>
            </View>
            <AddButton
                text={text}
                onPressButton={props.onPressButton}
                disableButton={disableButton}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        flexDirection: 'row',
        marginBottom: 20
    },
    nameContainer: {
        width: '70%'
    },
    titleText: {
        color: Colors.TEXT_COLOR,
        fontSize: 16,
        fontWeight: "100"
    },
    priceText: {
        color: Colors.ACCENT_COLOR,
        fontSize: 14,
        fontWeight: "100",
        marginTop: 3
    }
});


export default FoodListItem;