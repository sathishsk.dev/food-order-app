import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Colors} from "../styles/Colors";

import CommonStyles from "../styles/CommonStyles";
import AppIcon from "./AppIcon";
import Icons from "../styles/Icons";
import {ADD, REMOVE} from "../actions/ActionTypes";

const AddButton = ({text, onPressButton, disableButton}) => {

    const renderText = () => {
        let textStyle = styles.text;
        if (text === 'ADD') {
            textStyle = [textStyle, {width: '100%'}]
        }
        return (
            <Text style={textStyle}>
                {text}
            </Text>
        )
    };

    const renderAddIcon = () => {
        if (disableButton) {
            return (
                <TouchableOpacity
                    style={styles.addIcon}
                    onPress={() => onPressButton(ADD)}
                >
                    <AppIcon
                        name={Icons.PLUS}
                        color={Colors.TEXT_COLOR}
                        size={10}
                    />
                </TouchableOpacity>
            )
        }
    };

    const renderMinusIcon = () => {
        if (disableButton) {
            return (
                <TouchableOpacity
                    style={styles.minusIcon}
                    onPress={() => onPressButton(REMOVE)}
                >
                    <AppIcon
                        name={Icons.MINUS}
                        color={Colors.TEXT_COLOR}
                        size={10}
                    />
                </TouchableOpacity>
            )
        }
    };

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={() => onPressButton(ADD)}
            disabled={disableButton}
        >
            {renderMinusIcon()}
            {renderText()}
            {renderAddIcon()}
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        height: 28,
        flexDirection: 'row',
        position: 'absolute',
        end: 16,
        width: 100,
        borderColor: Colors.ACCENT_COLOR,
        borderWidth: 1,
        ...CommonStyles.centered
    },
    text: {
        color: Colors.TEXT_COLOR,
        fontSize: 14,
        textAlign: 'center',
        fontWeight: "900"
    },
    addIcon: {
        paddingHorizontal: 15,
        marginEnd: 15
    },
    minusIcon: {
        paddingHorizontal: 15,
        marginStart: 15
    }
});

export default AddButton;