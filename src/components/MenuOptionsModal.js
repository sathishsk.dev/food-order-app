import React, {useState} from 'react';
import Modal from 'react-native-modalbox';
import {View, StyleSheet, FlatList, TouchableOpacity, Text} from 'react-native';
import {Colors} from "../styles/Colors";
import CommonStyles from "../styles/CommonStyles";

const MenuOptionsModal = ({modalRef, options, onSelectMenu}) => {

    const [selectedOption, setSelectedOption] = useState('');

    const renderModalOptions = () => {
        return (
            <FlatList
                data={options}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={true}
                showsHorizontalScrollIndicator={false}
                renderItem={renderItem}
                ItemSeparatorComponent={renderItemSeparator}
                style={CommonStyles.flex}
            />
        )
    };

    const renderItemSeparator = () => {
        return <View style={styles.itemSeparator}/>;
    };

    const renderItem = ({item: menuOption, index}) => {
        let textStyle = styles.optionText;
        if (menuOption.title === selectedOption) {
            textStyle = styles.selectedOptionText;
        }
        return (
            <TouchableOpacity
                onPress={() => selectMenuItem(menuOption, index)}
                key={index}
                style={styles.menuItemStyle}
            >
                <Text style={textStyle}>
                    {menuOption.title}
                </Text>
                <Text style={styles.countText}>
                    {menuOption.data.length}
                </Text>
            </TouchableOpacity>
        )
    };

    const selectMenuItem = (menuOption, index) => {
        setSelectedOption(menuOption.title);
        onSelectMenu(index);
        modalRef?.current?.close();
    };

    return (
        <Modal
            ref={modalRef}
            position={'bottom'}
            style={styles.modalStyle}
            coverScreen
            backdrop
            backdropOpacity={0.5}
            backdropColor={Colors.BLACK}
        >
            <View style={CommonStyles.flex}>
                {renderModalOptions()}
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    modalStyle: {
        width: '70%',
        borderRadius: 10,
        bottom: 70,
        padding: 20,
        height: 185,
        maxHeight: 250
    },
    menuItemStyle: {
        flexDirection: 'row'
    },
    itemSeparator: {
        height: 15
    },
    optionText: {
        color: Colors.TEXT_COLOR,
        fontSize: 18,
        fontWeight: '100',
        flex: 1
    },
    selectedOptionText: {
        color: Colors.TEXT_COLOR,
        fontSize: 18,
        fontWeight: 'bold',
        flex: 1
    },
    countText: {
        color: Colors.ACCENT_COLOR,
        fontSize: 18,
        width: 50,
        textAlign: 'right',
        fontWeight: '100',
    }
});

export default MenuOptionsModal;
