import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';
import {Colors} from "../styles/Colors";
import DimensionsHelper from "../helpers/DimensionsHelper";
import AppIcon from "./AppIcon";
import Icons from "../styles/Icons";

const EmptyCartComponent = ({onPressButton}) => {
    return (
        <View style={styles.container}>
            <SafeAreaView>
                <View style={styles.iconStyle}>
                    <AppIcon size={100} color={Colors.BLACK} name={Icons.EMPTY}/>
                </View>
                <Text style={styles.labelStyle}>
                    {"Your cart is empty.\nAdd something from menu"}
                </Text>
                <TouchableOpacity
                    style={styles.buttonStyles}
                    onPress={onPressButton}
                >
                    <Text style={styles.textStyle}>
                        {"Browse Menu"}
                    </Text>
                </TouchableOpacity>
            </SafeAreaView>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width: DimensionsHelper.WIDTH,
        height: DimensionsHelper.HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE
    },
    buttonStyles: {
        height: 50,
        backgroundColor: Colors.PRIMARY_COLOR,
        width: 150,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    textStyle: {
        color: Colors.WHITE,
        fontSize: 18,
        fontWeight: "100",
        textAlign: 'center',
        width: '100%'
    },
    iconStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    labelStyle: {
        width: DimensionsHelper.WIDTH,
        marginBottom: 16,
        fontSize: 16,
        fontWeight: "100",
        textAlign: 'center',
        color: Colors.SECONDARY_TEXT_COLOR
    }
});


export default EmptyCartComponent;