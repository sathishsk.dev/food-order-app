import React from 'react';
import {StyleSheet, View, Text, SectionList, ScrollView, ImageBackground} from 'react-native';
import {Colors} from "../styles/Colors";
import CommonStyles from "../styles/CommonStyles";
import AppButton from "./AppButton";

const RestaurantCard = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.titleText}>
                {"Inka Restaurant"}
            </Text>
            <Text style={styles.label}>
                {"All days : 9:00 AM - 6:00 PM"}
            </Text>
            <Text style={[styles.label, styles.contactLabelMargin]}>
                {"Reach us at : 9854552412"}
            </Text>
            <AppButton
                buttonStyle={styles.buttonStyles}
                text={"Book A Table"}
                textStyle={styles.textStyle}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        padding: 16,
        marginTop: -40,
        marginHorizontal: 20,
        borderRadius: 5,
        backgroundColor: Colors.WHITE,
        ...CommonStyles.centered,
        ...CommonStyles.shadowView
    },
    titleText: {
        color: Colors.TITLE_COLOR,
        fontSize: 22,
        fontWeight: "900"
    },
    label: {
        color: Colors.TEXT_COLOR,
        fontSize: 14,
        marginTop: 20,
        fontWeight: "100"
    },
    contactLabelMargin: {
        marginTop: 6
    },
    buttonStyles: {
        borderRadius: 12,
        marginTop: 16,
        height: 40,
        width: '50%'
    },
    textStyle: {
        color: Colors.WHITE,
        fontSize: 16,
        fontWeight: "100"
    }
});


export default RestaurantCard;