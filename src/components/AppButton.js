import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Colors} from "../styles/Colors";
import CommonStyles from "../styles/CommonStyles";
import AppIcon from "./AppIcon";

const AppButton = ({buttonStyle, showIcon, iconSource, text, textStyle, onPressButton}) => {

    const renderIcon = () => {
        if (showIcon) {
            return (
                <AppIcon
                    name={iconSource}
                    color={Colors.WHITE}
                    size={25}
                />
            )
        }
    };

    const renderText = () => {
        let style = [styles.text, textStyle];
        if (!showIcon) {
            style = [style, {width: '100%', marginStart: 0}]
        }
        return (
            <Text style={style}>
                {text.toUpperCase()}
            </Text>
        )
    };

    return (
        <TouchableOpacity
            style={[styles.container, buttonStyle]}
            onPress={onPressButton}
        >
            {renderIcon()}
            {renderText()}
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        height: 55,
        flexDirection: 'row',
        backgroundColor: Colors.PRIMARY_COLOR,
        width: '100%',
        ...CommonStyles.centered
    },
    text: {
        color: Colors.WHITE,
        fontSize: 20,
        marginStart: 16,
        fontWeight: "900",
        textAlign: 'center'
    }
});

export default AppButton;