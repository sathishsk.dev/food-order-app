import { Dimensions } from "react-native";

const { height: windowHeight, width: windowWidth } = Dimensions.get("window");

const { height: screenHeight, width: screenWidth } = Dimensions.get("screen");

class DimensionsHelper {
    static HEIGHT = windowHeight;
    static WIDTH = windowWidth;
    static SCREEN_HEIGHT = screenHeight;
    static SCREEN_WIDTH = screenWidth;
}

export default DimensionsHelper;
