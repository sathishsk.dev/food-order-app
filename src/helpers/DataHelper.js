export const getCategoriesFromFoodItems = (foodItems) => {
    const categories = foodItems.map(foodItem => foodItem.category);
    return [...new Set(categories)]
};

export const computeSectionListItems = (foodItems, categories) => {
    return categories.map(category => {
        return {
            title: category,
            data: foodItems.filter(foodItem => foodItem.category === category)
        }
    })
};