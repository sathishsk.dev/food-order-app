import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RestaurantScreen from "../screens/RestaurantScreen";
import MyCartScreen from "../screens/MyCartScreen";
import {Colors} from "../styles/Colors";

const AppStackNavigator = () => {
    const AppStack = createStackNavigator();

    return (
        <NavigationContainer>
            <AppStack.Navigator>
                <AppStack.Screen
                    name="Restaurant"
                    component={RestaurantScreen}
                    options={{headerShown: false}}
                />
                <AppStack.Screen
                    name="MyCart"
                    component={MyCartScreen}
                    options={{
                        headerStyle: {
                            backgroundColor: Colors.PRIMARY_COLOR,
                            elevation: 0,
                            shadowOpacity: 0,
                        },
                        headerTintColor: Colors.WHITE
                    }}
                />
            </AppStack.Navigator>
        </NavigationContainer>
    )
};

export default AppStackNavigator;