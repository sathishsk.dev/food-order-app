import React, {useEffect, useState, createRef} from 'react';
import {StyleSheet, View, Text, SectionList, ScrollView, ImageBackground, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {computeSectionListItems, getCategoriesFromFoodItems} from "../helpers/DataHelper";
import CommonStyles from "../styles/CommonStyles";
import {Colors} from "../styles/Colors";
import AppButton from "../components/AppButton";
import RestaurantCard from "../components/RestaurantCard";
import FoodListHeader from "../components/FoodListHeader";
import FoodListItem from "../components/FoodListItem";
import {ADD} from "../actions/ActionTypes";
import {addQuantity, subtractQuantity} from "../actions/Actions";
import AppIcon from "../components/AppIcon";
import Icons from "../styles/Icons";
import MenuOptionsModal from "../components/MenuOptionsModal";

const RestaurantScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const cartReducer = useSelector((state) => state.CartReducer);
    const {foodItems, cartItems} = cartReducer;
    const [sectionListItems, setSectionListItems] = useState([]);
    let modalRef = createRef();
    let sectionListRef = createRef();

    useEffect(() => {
        getExpandableListData();
    }, []);

    const getExpandableListData = () => {
        const categories = getCategoriesFromFoodItems(foodItems);
        setSectionListItems(computeSectionListItems(foodItems, categories));
    };

    const renderCoverImage = () => {
        return (
            <ImageBackground
                style={styles.coverImage}
                source={require('../../assets/images/cover.png')}
            />
        )
    };

    const renderRestaurantCard = () => {
        return (
            <RestaurantCard/>
        )
    };

    const renderFoodItemList = () => {
        let style = styles.sectionList;
        if (cartItems.length > 0) {
            style = [style, {marginBottom: 60}]
        }
        return (
            <SectionList
                ref={ref => {
                    sectionListRef = ref;
                }}
                sections={sectionListItems}
                keyExtractor={(item, index) => item + index}
                renderItem={({item}) => renderItem(item)}
                renderSectionHeader={({section: {title}}) => renderSectionHeader(title)}
                style={style}
                ListHeaderComponent={renderRestaurantInfoView}
            />
        )
    };

    const renderRestaurantInfoView = () => {
        return (
            <>
                {renderCoverImage()}
                {renderRestaurantCard()}
            </>
        )
    };

    const renderSectionHeader = (title) => {
        return <FoodListHeader title={title}/>
    };

    const renderItem = (item) => {
        return <FoodListItem
            key={item.foodName}
            item={item}
            onPressButton={(action) => onPressAddButton(item, action)}
        />
    };

    const onPressAddButton = (item, action) => {
        const {id, quantity} = item;
        if (action === ADD) {
            if (quantity < 20) {
                dispatch(addQuantity(id))
            } else {
                alert("You reached maximum quantity limit. You can't add more than 20 quantities")
            }
        } else {
            dispatch(subtractQuantity(id))
        }
    };

    const renderViewCartButton = () => {
        if (cartItems.length > 0) {
            const text = `View Cart (${cartItems.length} Items)`;
            return (
                <AppButton
                    buttonStyle={styles.buttonStyle}
                    text={text}
                    onPressButton={() => navigation.navigate("MyCart")}
                />
            )
        }
    };

    const renderMenuButton = () => {
        let style = styles.menuButton;
        if (cartItems.length > 0) {
            style = [style, {bottom: 70}]
        }
        return (
            <TouchableOpacity
                style={style}
                onPress={openMenuModal}
            >
                <AppIcon
                    name={Icons.MENU}
                    color={Colors.WHITE}
                    size={20}/>
                <Text style={styles.menuText}>
                    {"MENU"}
                </Text>
            </TouchableOpacity>
        )
    };

    const openMenuModal = () => {
        modalRef?.current?.open();
    };

    const renderMenuModal = () => {
        return (
            <MenuOptionsModal
                ref={modalRef}
                modalRef={modalRef}
                options={sectionListItems}
                onSelectMenu={onSelectMenu}
            />
        )
    };

    const onSelectMenu = (index) => {
        sectionListRef.scrollToLocation({
            animated: true,
            itemIndex: 0,
            sectionIndex: index,
        });
    };

    return (
        <View style={styles.container}>
            {renderFoodItemList()}
            {renderMenuModal()}
            {renderMenuButton()}
            {renderViewCartButton()}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: Colors.WHITE
    },
    coverImage: {
        flex: 1,
        minHeight: 200
    },
    buttonStyle: {
        position: 'absolute',
        bottom: 0
    },
    menuButton: {
        width: 120,
        backgroundColor: Colors.ACCENT_COLOR,
        height: 40,
        borderRadius: 20,
        position: 'absolute',
        alignSelf: 'center',
        bottom: 20,
        ...CommonStyles.centered,
        ...CommonStyles.flexRow,
        ...CommonStyles.shadowView,
        elevation: 2
    },
    menuText: {
        color: Colors.WHITE,
        fontSize: 16,
        fontWeight: "900",
        marginStart: 8,
        width: '40%'
    },
    sectionList: {}
});

export default RestaurantScreen;

