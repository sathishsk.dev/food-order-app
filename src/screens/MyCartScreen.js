import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, FlatList, ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import CommonStyles from "../styles/CommonStyles";
import {Colors} from "../styles/Colors";
import AppButton from "../components/AppButton";
import FoodListItem from "../components/FoodListItem";
import {ADD} from "../actions/ActionTypes";
import {addQuantity, subtractQuantity} from "../actions/Actions";
import DimensionsHelper from "../helpers/DimensionsHelper";
import EmptyCartComponent from "../components/EmptyCartComponent";

const MyCartScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const cartReducer = useSelector((state) => state.CartReducer);
    const {cartItems, totalCost} = cartReducer;
    const [enableShowMore, setEnableShowMore] = useState(false);
    const [itemsCount, setItemsCount] = useState(2);

    useEffect(() => {
        if (cartItems.length > 2) {
            setEnableShowMore(true)
        }
    }, []);

    const renderHeader = () => {
        return (
            <>
                <View style={styles.header}>
                    {renderTotalCostView()}
                </View>
                <Text style={styles.reviewOrder}> {"Review Orders"} </Text>
            </>
        )
    };

    const renderTotalCostView = () => {
        return (
            <View style={styles.totalCostContainer}>
                <Text style={styles.totalCostTitle}>
                    {"Total Cost"}
                </Text>
                <Text style={styles.totalCostValue}>
                    {`$${totalCost}`}
                </Text>
            </View>
        )
    };

    const renderCartItemsView = () => {
        return (
            <View style={CommonStyles.flex}>
                <FlatList
                    data={cartItems.slice(0, itemsCount)}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    renderItem={renderItem}
                    ItemSeparatorComponent={renderItemSeparator}
                    style={{marginBottom: 60}}
                    ListFooterComponent={renderShowMoreText}
                    ListHeaderComponent={renderHeader}
                />
            </View>
        )
    };

    const renderShowMoreText = () => {
        if (enableShowMore && itemsCount === 2) {
            return (
                <>
                    {renderItemSeparator()}
                    <Text
                        style={styles.showMore}
                        onPress={() => {
                            setEnableShowMore(false);
                            setItemsCount(cartItems.length);
                        }}
                    >
                        {"Show more"}
                    </Text>
                </>
            )
        } else {
            return renderItemSeparator()
        }
    };

    const renderItem = ({item}) => {
        return (
            <View style={styles.itemMargin}>
                <FoodListItem
                    key={item.foodName}
                    item={item}
                    onPressButton={(action) => onPressAddButton(item, action)}
                />
            </View>
        )
    };

    const renderItemSeparator = () => {
        return <View style={styles.itemSeparator}/>;
    };

    const onPressAddButton = (item, action) => {
        const {id, quantity} = item;
        if (action === ADD) {
            if (quantity < 20) {
                dispatch(addQuantity(id))
            } else {
                alert("You reached maximum quantity limit. You can't add more than 20 quantities")
            }
        } else {
            dispatch(subtractQuantity(id))
        }
    };

    const renderPlaceOrderButton = () => {
        return (
            <AppButton
                buttonStyle={styles.buttonStyle}
                text={"PLACE ORDER"}
                onPressButton={() => alert("Order Placed Successfully")}
            />
        )
    };

    if (cartItems.length === 0) {
        return <EmptyCartComponent onPressButton={() => navigation.goBack()}/>
    }

    return (
        <View style={styles.container}>
            {renderCartItemsView()}
            {renderPlaceOrderButton()}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: DimensionsHelper.WIDTH,
        height: DimensionsHelper.HEIGHT,
        backgroundColor: Colors.WHITE
    },
    header: {
        height: DimensionsHelper.HEIGHT / 3 - 50,
        backgroundColor: Colors.PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 16
    },
    buttonStyle: {
        position: 'absolute',
        bottom: 0
    },
    totalCostContainer: {
        ...CommonStyles.shadowView,
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        padding: 16,
        marginHorizontal: 20,
        borderRadius: 12,
        backgroundColor: Colors.WHITE,
    },
    totalCostTitle: {
        color: Colors.ACCENT_COLOR,
        fontSize: 18,
        fontWeight: '100'
    },
    totalCostValue: {
        color: Colors.TEXT_COLOR,
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        width: '100%',
        textAlign: 'center'
    },
    itemSeparator: {
        height: 2,
        marginVertical: 16,
        marginHorizontal: 16,
        marginTop: -5,
        backgroundColor: Colors.GREY
    },
    reviewOrder: {
        color: Colors.TEXT_COLOR,
        fontSize: 18,
        fontWeight: 'bold',
        marginStart: 10,
        marginBottom: 25
    },
    showMore: {
        color: Colors.PRIMARY_COLOR,
        fontSize: 16,
        textAlign: 'right',
        width: '100%',
        textDecorationLine: 'underline',
        paddingEnd: 16
    },
    itemMargin: {
        marginHorizontal: 16
    }
});

export default MyCartScreen;

